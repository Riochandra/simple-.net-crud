﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DataSiswa
{
    public partial class Form1 : Form
    {
        public string idterpilih;
        SqlConnection koneksi = new SqlConnection(DataSiswa.Properties.Resources.koneksi.ToString());
        public SqlCommand cmd;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            tampilkandata();
        }
        private void tampilkandata ()
        {
            SqlDataAdapter DA = new SqlDataAdapter("select * from siswa", koneksi);
            DataSet DS = new DataSet();
            DA.Fill(DS);
            DGSiswa.DataSource = DS.Tables[0];
        }

        private void refresh()
        {
            koneksi.Close();
            tampilkandata();
            tambah();
            ubah();
            hapus();
            cari.Clear();
            NIS.Clear();
            Nama.Clear();
            Kelas.Clear();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            tambah();
        }

        private void tambah()
        {
            koneksi.Open();
            cmd = new SqlCommand("insert into siswa values('" + NIS.Text + "','" + Nama.Text + "','" + Kelas.Text + "')", koneksi);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Anda berhasil menambahkan data baru", "informasi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            koneksi.Close();
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            ubah();
        }

        private void ubah()
        {
            koneksi.Open();
            cmd = new SqlCommand("update siswa set NIS='" + NIS.Text + "',Nama='" + Nama.Text + "',Kelas='" + Kelas.Text + "' where id='" + idterpilih + "'", koneksi);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Anda berhasil mengubah data", "informasi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            koneksi.Close();
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            hapus();
        }

        private void hapus()
        {
            koneksi.Open();
            cmd = new SqlCommand("delete from siswa where id='" + idterpilih + "'", koneksi);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Anda berhasil menghapus data", "informasi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            koneksi.Close();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            tampilkandata();
        }

        private void Cari_TextChanged(object sender, EventArgs e)
        {
            Cari();
        }

        private void Cari()
        {
            SqlDataAdapter DA = new SqlDataAdapter("select * from siswa where nama LIKE'%"+cari.Text+"%'", koneksi);
            DataSet DS = new DataSet();
            DA.Fill(DS);
            DGSiswa.DataSource = DS.Tables[0];
        }
    }
    }
